.. faas-functions

PiperCI Tasks - OpenFaaS
========================
# Working with PiperCI Functions

PiperCI utilizes OpenFaaS as it's primary Task runner backend, however the python libraries are designed to support any python environment backend, and the RESTful APIs may be implemented in any desired language.

.. toctree::
   :maxdepth: 1
   :caption: Provided OpenFaaS functions in this release
   :glob:

   *-faas/README
   faas-templates/README

## Developing Your Own PiperCI Function

The most basic PiperCI function example is the :doc:`noop-faas </noop-faas/README>`. Like it's name implies it does nothing but register itself to GMan and record a simple log. However it gives a good basic example on how to interact with the PiperCI python library.

A more complete working example would be the :doc:`flake8 faas </flake8-faas/README>` Task. This function runs a your source against the flake8 python linter.

Also provided are the base :doc:`FaaS function templates </faas-templates/README>`. These templates are basically Docker images that have been prepared to work with PiperCI's API's. These should be used as the basis for PiperCI functions.


## Deploying Your PiperCI Function

### Development Install

A development installation will install the PiperCI environment on your local workstation.

The supported development environment uses Docker-in-Docker to install PiperCI inside of an isolated
docker environment which can be easily reset. This installation method will required that Docker is installed on your development machine.

Detailed installation instructions can be found here: https://gitlab.com/dreamer-labs/piperci/piperci-installer/-/tree/master/dev

Once the PiperCI environment is deployed you can run `faas deploy` on your Function's `stack.yml` file to deploy
your function to your dev PiperCI environment.

For example, to add the flake8 FaaS to your local PiperCI development environment

```
git clone https://gitlab.com/dreamer-labs/piperci/piperci-flake8-faas.git
cd piperci-flake8-faas.git
export DOCKER_TLS=True
export DOCKER_HOST=tcp://172.17.0.2:2376
export DOCKER_CERT_PATH=/var/lib/docker/volumes/certs/_data
## Edit stack.yml and replace the "gateway" url with http://172.17.0.2:8080
faas build
faas deploy
```

Your new function should now be available inside of the PiperCI development environment.

#### Testing your new function

In order to test your new function you will need to install PiCli. This can be done by pulling down the PiCli docker image or installing the python package.

```
## Docker image
docker pull registry.gitlab.com/dreamer-labs/piperci/piperci-picli:latest

## Python package
pip install git+https://gitlab.com/dreamer-labs/piperci/piperci-picli.git#master
```

Next you will need to pull down the `piperci-demo` repository. This repository can be used as a reference PiperCI-enabled repository.

```
git clone https://gitlab.com/dreamer-labs/piperci/piperci-demo.git
cd piperci-demo
```

Now edit `piperci.d/default/config.yml` to point to your PiperCI environment. Usually this will look like the following, however you will
need to double check the IP address given to your PiperCI environment.

```
# piperci.d/default/config.yml
---
project_name: "python_project"
version: "0.0.0"
gman_url: "http://172.17.0.2:8089"
faas_endpoint: "http://172.17.0.2:8080"
storage:
  type: "minio"
  hostname: "172.17.0.2:9000"
  access_key: "test"
  secret_key: "test_secret"
```

Next you can edit your `piperci.d/default/stages.yml` file to point to your new FaaS function.

```
# piperci.d/default/stages.yml
---
stages:
  - name: my_new_stage
    deps:
    tasks:
      - name: my_new_function
        uri: /function/piperci-my-new-function
        config:
          my_config_values: test
```


Finally, run PiCli against the demo repository.

```
# Docker picli installation
docker run -t -v ${PWD}:/app registry.gitlab.com/dreamer-labs/piperci/piperci-picli:latest



### Production Install

To install a production PiperCI environment you can use the `piperci-installer` repository located here: https://gitlab.com/dreamer-labs/piperci/piperci-installer/-/tree/master/

This repository contains a `configure.yml` playbook which will look in the running environment for the following environment variables:

  `read_releases_url`
  `read_releases_version`

These two environment variables control your environment's installation configuration. An example release definition file can be found here:
https://gitlab.com/dreamer-labs/piperci/piperci-releases/-/blob/master/altstadt/installer.yml

FaaS functions can be defined in the `faas` block of the configuration file. This is where your production FaaS definitions will live. Repeated
runs of this Ansible playbook will ensure that the FaaS functions defined in this block are running on your PiperCI infrastructure.

Detailed documentation on the installation procedures can be found in the README of the piperci-installer repository.
