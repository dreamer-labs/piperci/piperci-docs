# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# http://www.sphinx-doc.org/en/master/config

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
# import os
# import sys
# sys.path.insert(0, os.path.abspath('.'))
import os
import sys

import sphinx_theme


sys.path.append(os.path.abspath('../sphinx_ext/'))

# -- Project information -----------------------------------------------------

project = 'piperci'
copyright = '2019, Global InfoTek, Inc'
author = 'Global InfoTek, Inc.'

# The full version, including alpha/beta/rc tags
release = 'altstadt'


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    'sphinx.ext.autodoc',
    'sphinxcontrib.autoprogram',
    'sphinxcontrib.openapi',
    'toctree_filter',
    'sphinx_click.ext'
]

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = []


# -- Options for HTML output -------------------------------------------------

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']

# ---sphinx-themes-----
html_theme = "stanford_theme"
html_theme_path = [sphinx_theme.get_html_theme_path('stanford-theme')]


def setup(app):
    app.add_stylesheet('css/overrides.css')
    app.add_js_file('js/float_pipe.js')
