.. piperci documentation master file, created by
   sphinx-quickstart on Tue Jul 30 16:35:02 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to the PiperCI documentation!
=====================================

Overview
--------

PiperCI is a CI/CD management framework that manages the pipelines, stages, tasks, resources, and infrastructure typically associated with software CI/CD. The framework includes both client-side components (software installed on an individual user’s workstation or within an existing CI/CD pipeline) and server-side components (remote infrastructure or function endpoints) that handles everything from project bootstrapping to software deployment.

Project Goals
-------------

The PiperCI platform aims to
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

- allow the running of pipelines before pushing code into a source code manager (SCM)
- provide CI/CD infrastructure as if it were on the local workstation using a simple CLI interface
- work with a traditional/existing CI/CD orchestrator pipelines only replacing the logic that PiperCI handles better
- bolt-on to existing workflows easily with minimal effort
- aid in the transfer of pipelines to different CI/CD orchestrators while still getting the same result
- allow the running of individual pipeline stages against local code (think `tox -e lint` remotely)
- eliminate the "it works on my machine" problem

Project goals and guiding principles
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

- Have good sensible defaults based on familiar common standards
- Stages should be callable at any point manually or as part of a pipeline
- Strive for Simplicity and Speed
- The infrastructure should be installable and upgradable easily and quickly, and should be tested as part of the codebase
- Functions should require as little client-side configuration as possible
- Functions should validate inputs to resources as its first step, to fail early if there input would cause a fatal issue in a later step
- Functions should be versioned when called to allow for multiple versions to exist in the same deployment, allowing for more precise control over the what code is running in the pipeline.

Default Implementation
----------------------

PiperCI's primary component is the PiCli the PiperCI CLI which parses and controls the execution flow for CI/CD pipelines. Provided PiperCI Tasks are written as OpenFaaS functions and deployed using the provided standalone `OpenFaaS <https://www.openfaas.com/>`__ deployment. However, PiperCI Tasks may be implemented in any language so long as they conform to the PiperCI Task Manager and Task APIs. Process state is maintained by :doc:`GMan </task_manager/README>`, and artifacts are stored in `MinIO <https://min.io/download>`__

.. image:: piperci-process-flow.png

Deployment
----------

:doc:`Piperci Installer </piperci-installer/README>`

.. toctree-filter::
   :maxdepth: 2
   :caption: Contents:
   :glob:
   :exclude-patterns: .*-faas/README faas-templates/README

   piperci-task-api
   piperci-task-manager-api

   piperci-functions
   */README
   project-info/LICENSE
   project-info/contributing



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
